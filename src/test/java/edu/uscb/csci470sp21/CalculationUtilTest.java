package edu.uscb.csci470sp21;

import static org.junit.Assert.*;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains methods for testing the arithmetic utility
 * methods defined in the CalculationUtil class
 * 
 * @author jaronm@email.uscb.edu
 * @version 3/7/2021
 */
public class CalculationUtilTest {

	private Logger logger = LoggerFactory.getLogger( CalculationUtilTest.class);
	
	@Test
	public void testAdd() {
		double value1 = 25;
		double value2 = 10;
		double expectedResult = 35;
		double tolerance = 0.00005;
		double testResult = CalculationUtil.add( value1, value2);
		logger.info("testAdd expected: " + expectedResult + ", actual: " + testResult);
		assertEquals( expectedResult, testResult, tolerance );
	} // end method testAdd

	@Test
	public void testSubtract() {
		double value1 = 25;
		double value2 = 10;
		double expectedResult = 15;
		double tolerance = 0.00005;
		double testResult = CalculationUtil.subtract( value1, value2);
		logger.info("testSubtract expected: " + expectedResult + ", actual: " + testResult );
		assertEquals( expectedResult, testResult, tolerance);
	} // end test method testSubtract

	@Test
	public void testMultiply() {
		double value1 = 25;
		double value2 = 10;
		double expectedResult = 250;
		double tolerance = 0.00005;
		double testResult = CalculationUtil.multiply( value1, value2);
		logger.info("testMultiply expected: " + expectedResult + ", actual: " + testResult );
		assertEquals( expectedResult, testResult, tolerance);
	} // end test method testMultiply

	@Test
	public void testDivide() {
		double value1 = 25;
		double value2 = 10;
		double expectedResult = 2.5;
		double tolerance = 0.00005;
		double testResult = CalculationUtil.divide( value1, value2);
		logger.info("testDivide expected: " + expectedResult + ", actual: " + testResult );
		assertEquals( expectedResult, testResult, tolerance);
	} // end test method testDivide

	@Test
	public void testLongDivision() {
		int dividend = 25;
		int divisor = 10;
		String expectedResult = "2R5";
		String testResult = CalculationUtil.longDivision( divisor, dividend );
		logger.info("testLongDivision expected: " + expectedResult + ", actual: " + testResult );
		assertTrue( expectedResult.equals(testResult) );
	} // end test method testLongDivision
	
} // end class CalculationUtilTest
