<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="includes/header.jsp" />
	
<!-- main content starts here -->
<main>
  <div id="container">
    <form action="calculate" method="post">
      <input type="hidden" name="action" value="calculate">
      <div class="flex-container-outer">

        <div class="flex-container-inner">
          <div class="div-label"><label>Enter<br>First Number:</label></div>
          <div class="div-input"><input type="text" name="firstNumber"></div>
        </div>

        <div class="flex-container-inner">
          <div class="div-label"><label>Choose an<br>Operation:</label></div>
          <div class="div-input">
            <select name="operation">
              <option value="add">+</option>
              <option value="subtract">-</option>
              <option value="multiply">&times;</option>
              <option value="divide">&divide;</option>
              <option value="longDivision">&#x27CC;</option>
              </option>
            </select>
          </div>
        </div>

        <div class="flex-container-inner">
          <div class="div-label"><label>Enter<br>Second Number:</label></div>
          <div class="div-input"><input type="text" name="secondNumber"></div>
        </div>

      </div> <!-- end of flex-container-outer -->
      <input type="submit" value="Calculationate!" class="large-button">
    </form>

	<!-- TODO: replace hard-coded result text 
         with Expression Language (EL) expression -->
    <div id="result">2 + 3 = 5</div>

    <!-- placeholder for user calculation history -->

  </div> <!-- end of id="container" -->
</main>

<c:import url="includes/footer.jsp" />

</body>
</html>